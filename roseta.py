#!/usr/bin/python
# roseta.py ---
#
# Filename: roseta.py
# Description: RosetaCode API
# Author: guilhem
# Maintainer: -
# Created: Mon Dec  1 10:26:36 2014 (-0500)
# Version:
# Package-Requires: (urllib2, re, HTMLParser, sys)
# Last-Updated:
#           By:
#     Update #: 2
# URL: https://bitbucket.org/EddyMcCoy/rostacodeapi
# Doc URL: -
# Keywords: -
# Compatibility: python 2.7

# Commentary:
# RosetaCode API
# use : roseta.py [function] [param]
#
# 	+ function: [get algo], [list algo], [list lang]

# Change Log:
#
#
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.
#
#

# Code:

import urllib2, re, HTMLParser, sys

class RosettaCode(object):

    def __init__(self):
        self.uri = 'http://rosettacode.org'
        self.h = HTMLParser.HTMLParser()
        self.agent = {'User-agent' : 'Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5'}
        self._r_list = re.compile(ur'<li>\s*<a\s+href="(.*?)".*>(.*)\s*<\/a>\s*<\/li>', re.I | re.M | re.U)
        self._r_span = re.compile(ur'<span.*?>(.*?)<\/span>(.*?)(?=<span|</?a|</?pre)', re.I | re.M | re.U)

    def _fetch(self, path):
        req = urllib2.Request(self.uri + path, None, self.agent)
        return  urllib2.urlopen(req).read()

    def _fetchPageList(self, path):
        f = self._fetch(path)
        return re.findall(self._r_list, f) if f else []

    def _fetchLang(self):
        return self._fetchPageList('/wiki/Category:Programming_Languages')

    def _fetchAlgo(self, lang):
        if lang and lang != '':
            for l in self._fetchLang():
                if l[1].lower() == lang.lower():
                    return self._fetchPageList(l[0])
        return list()

    def listLang(self):
        return "\n".join([l[1] for l in self._fetchLang()])


    def listAlgo(self, lang):
        return "\n".join([a[1] for a in self._fetchAlgo(lang)])

    def getAlgo(self, lang, algo):
        for j in self._fetchAlgo(lang):
            if algo.lower() in j[1].lower():
                f = re.search(ur'<h2>.*?id="' + re.escape(lang) + '".*?</h2>.*?<pre.*?>(.*?)</pre>', self._fetch(j[0]), re.I | re.M | re.U | re.S)
                return re.sub(r'<\s*br\s*/\s*>', '\n', (''.join([self.h.unescape(word[0] + word[1]).encode('ascii', 'ignore') for word in re.findall(self._r_span, f.group(1))])), 0, re.I | re.M | re.U) if f else ''

    def call(self, call, arg):
        try:
            ret = getattr(self, call)(*arg)
            return ret if type(ret) is str else None
        except Exception as e:
            print e

if __name__ == "__main__":
    if (len(sys.argv) > 1):
        r = RosettaCode()
        callback = "".join([a.title() for a in sys.argv[1].split(' ')])
        callback = callback[:1].lower() + callback[1:] if callback else ''
        print r.call(callback, sys.argv[2:])

#
# roseta.py ends here
